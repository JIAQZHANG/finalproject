package epg.model;

import static epg.StartupConstants.DEFAULT_EPORTFOLIO_TITLE;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import epg.view.EPortfolioGeneratorView;

/**
 * This class manages all the data associated with a slideshow.
 * 
 * @author McKilla Gorilla & _____________
 */
public class EPortfolioModel {
    EPortfolioGeneratorView ui;
    String studentName;
    ObservableList<Page> pages;
    Page selectedPage;
    
    public EPortfolioModel(EPortfolioGeneratorView initUI) {
	ui = initUI;
	pages = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isPageSelected() {
	return selectedPage != null;
    }
    
    public boolean isSelectedPage(Page testPage) {
	return selectedPage == testPage;
    }
    
    public ObservableList<Page> getPages() {
	return pages;
    }
    
    public Page getSelectedPage() {
	return selectedPage;
    }

    public void setSelectedPage(Page initSelectedPage) {
	selectedPage = initSelectedPage;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    
    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no pages and a default studentName.
     */
    public void reset() {
	pages.clear();
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
	studentName = DEFAULT_EPORTFOLIO_TITLE;
	selectedPage = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param font
     * @param layout
     * @param color
     * @param pageName
     * @param elements
     * @param bannerImageSrc
     * @param footer
     */
    public void addPage(String font, String layout, String color, String pageName, ObservableList<Element> elements, String bannerImageSrc, String footer) {
	Page pageToAdd = new Page(font, layout, color, pageName, elements, bannerImageSrc, footer);
	pages.add(pageToAdd);
	ui.reloadSiteToolbarPane();
    }

    /**
     * Removes the currently selected slide from the slide show and
     * updates the display.
     */
    public void removeSelectedPage() {
	if (isPageSelected()) {
	    pages.remove(selectedPage);
	    selectedPage = null;
	    ui.reloadSiteToolbarPane();
	}
    }

    public void addPage() {
        Page pageToAdd = new Page();
        pages.add(pageToAdd);
        ui.reloadSiteToolbarPane();
    }

}