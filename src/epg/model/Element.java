/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import java.util.ArrayList;
import ssm.model.SlideShowModel;

/**
 *
 * @author JIAQI ZHANG
 */
public class Element {
    private String type;
    private String text;
    private String textFont;
    private ArrayList<String> list;
    private int height;
    private int width;
    private String caption;
    private String floating;
    private String source;
    private SlideShowModel slideShow;

    public Element(String type, String text, String textFont, ArrayList<String> list, int height, int width, String caption, String floating, String source, SlideShowModel slideShow) {
        this.type = type;
        this.text = text;
        this.textFont = textFont;
        this.list = list;
        this.height = height;
        this.width = width;
        this.caption = caption;
        this.floating = floating;
        this.source = source;
        this.slideShow = slideShow;
    }

    
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextFont() {
        return textFont;
    }

    public void setTextFont(String font) {
        this.textFont = font;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void setList(ArrayList<String> list) {
        this.list = list;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getFloating() {
        return floating;
    }

    public void setFloating(String floating) {
        this.floating = floating;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public SlideShowModel getSlideShow() {
        return slideShow;
    }

    public void setSlideShow(SlideShowModel slideShow) {
        this.slideShow = slideShow;
    }
    
    
    
}
