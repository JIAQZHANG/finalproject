/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.model;

import static epg.StartupConstants.DEFAULT_PAGE_NAME;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author JIAQI ZHANG
 */
public class Page {
    String font;
    String layout;
    String color;
    String pageName;
    ObservableList<Element> elements;
    String bannerImageSrc;
    String footer;
        

    public Page() {
        pageName = DEFAULT_PAGE_NAME;
        elements = FXCollections.observableArrayList();
        font = "";
        layout = "";
        color = "";
        bannerImageSrc = "";
        footer = "";
    }

    public Page(String font, String layout, String color, String pageName, ObservableList<Element> elements, String bannerImageSrc, String footer) {
        this.font = font;
        this.layout = layout;
        this.color = color;
        this.pageName = pageName;
        this.elements = elements;
        this.bannerImageSrc = bannerImageSrc;
        this.footer = footer;
    }
    
    

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public ObservableList<Element> getElements() {
        return elements;
    }

    public void setElements(ObservableList<Element> content) {
        this.elements = content;
    }

    public String getBannerImageSrc() {
        return bannerImageSrc;
    }

    public void setBannerImageSrc(String bannerImageSrc) {
        this.bannerImageSrc = bannerImageSrc;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }


    
    




    
}
