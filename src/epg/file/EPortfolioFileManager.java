package epg.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import static epg.StartupConstants.*;
import epg.model.Element;
import epg.model.Page;
import epg.model.EPortfolioModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ssm.file.SlideShowFileManager;
import ssm.model.SlideShowModel;

/**
 * This class uses the JSON standard to read and write ePortfolio data files.
 *
 * @author McKilla Gorilla & _____________
 */
public class EPortfolioFileManager {

    // JSON FILE READING AND WRITING CONSTANTS
    public static String JSON_STUDENT_NAME = "student_name";
    public static String JSON_PAGES = "pages";
    public static String JSON_PAGE_NAME = "page_name";
    public static String JSON_FONT = "font";
    public static String JSON_LAYOUT = "layout";
    public static String JSON_COLOR = "color";
    public static String JSON_ELEMENT = "element";
    public static String JSON_BANNER_IMAGE_SOURCE = "banner_image_source";
    public static String JSON_FOOTER = "footer";
    public static String JSON_TYPE = "type";
    public static String JSON_TEXT_FONT = "text_font";
    public static String JSON_TEXT = "text";
    public static String JSON_LIST = "list";
    public static String JSON_HEIGHT = "height";
    public static String JSON_WIDTH = "width";
    public static String JSON_CAPTION = "caption";
    public static String JSON_FLOATING = "floating";
    public static String JSON_SOURCE = "source";
    public static String JSON_SLIDESHOW = "slide_show";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";

    /**
     * This method saves all the data associated with a slide show to a JSON
     * file.
     *
     * @param ePortfolioToSave The course whose data we are saving.
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    public void saveEPortfolio(EPortfolioModel ePortfolioToSave) throws IOException {
        StringWriter sw = new StringWriter();

        // BUILD THE SLIDES ARRAY
        JsonArray slidesJsonArray = makePagesJsonArray(ePortfolioToSave.getPages());

        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject ePortfolioJsonObject = Json.createObjectBuilder()
                .add(JSON_STUDENT_NAME, ePortfolioToSave.getStudentName())
                .add(JSON_PAGES, slidesJsonArray)
                .build();

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);

        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(ePortfolioJsonObject);
        jsonWriter.close();

        // INIT THE WRITER
        String ePortfolioName = "" + ePortfolioToSave.getStudentName();
        String jsonFilePath = PATH_EPORTFOLIOS + SLASH + ePortfolioName + JSON_EXT;
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(ePortfolioJsonObject);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(jsonFilePath);
        pw.write(prettyPrinted);
        pw.close();
        System.out.println(prettyPrinted);
    }

    /**
     * This method loads the contents of a JSON file representing a slide show
     * into a SlideSShowModel object.
     *
     * @param ePortfolioToLoad The slide show to load
     * @param jsonFilePath The JSON file to load.
     * @throws IOException
     */
    public void loadEPortfolio(EPortfolioModel ePortfolioToLoad, String jsonFilePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(jsonFilePath);

        // NOW LOAD THE COURSE
        ePortfolioToLoad.reset();
        ePortfolioToLoad.setStudentName(json.getString(JSON_STUDENT_NAME));
        JsonArray jsonPagesArray = json.getJsonArray(JSON_PAGES);
        for (int i = 0; i < jsonPagesArray.size(); i++) {
            JsonObject pageJso = jsonPagesArray.getJsonObject(i);

            JsonArray jsonElementsArray = json.getJsonArray(JSON_ELEMENT);
            ObservableList<Element> elements = FXCollections.observableArrayList();

            if (jsonElementsArray != null) {
                for (int j = 0; j < jsonElementsArray.size(); j++) {
                    JsonObject elementJso = jsonPagesArray.getJsonObject(j);
                    JsonArray listJso = elementJso.getJsonArray(JSON_LIST);
                    ArrayList<String> list = new ArrayList();
                    for (int k = 0; k < listJso.size(); k++) {
                        list.add(listJso.getString(k));
                    }

                    Element element = new Element(elementJso.getString(JSON_TYPE),
                            elementJso.getString(JSON_TEXT),
                            elementJso.getString(JSON_TEXT_FONT),
                            list,
                            elementJso.getInt(JSON_HEIGHT),
                            elementJso.getInt(JSON_WIDTH),
                            elementJso.getString(JSON_CAPTION),
                            elementJso.getString(JSON_FLOATING),
                            elementJso.getString(JSON_SOURCE),
                            new SlideShowFileManager().loadSlideShowJson(elementJso.getJsonObject(JSON_SLIDESHOW)));

                    elements.add(element);
                }
            }

            ePortfolioToLoad.addPage(pageJso.getString(JSON_FONT),
                    pageJso.getString(JSON_LAYOUT),
                    pageJso.getString(JSON_COLOR),
                    pageJso.getString(JSON_PAGE_NAME),
                    elements,
                    pageJso.getString(JSON_BANNER_IMAGE_SOURCE),
                    pageJso.getString(JSON_FOOTER));
        }
    }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }

    private JsonArray makePagesJsonArray(List<Page> pages) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Page page : pages) {
            JsonObject jso = makePageJsonObject(page);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject makePageJsonObject(Page page) {
        JsonArray elementJsonArray = makeElementJsonArray(page.getElements());
        for (Element element : page.getElements()) {
            makeElementJsonObject(element);
        }

        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_PAGE_NAME, page.getPageName())
                .add(JSON_FONT, page.getFont())
                .add(JSON_COLOR, page.getColor())
                .add(JSON_LAYOUT, page.getLayout())
                .add(JSON_FOOTER, page.getFooter())
                .add(JSON_BANNER_IMAGE_SOURCE, page.getBannerImageSrc())
                .add(JSON_ELEMENT, elementJsonArray)
                .build();
        return jso;
    }

    private JsonArray makeElementJsonArray(List<Element> elements) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Element element : elements) {
            JsonObject jso = makeElementJsonObject(element);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonObject makeElementJsonObject(Element element) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String str : element.getList()) {
            jsb.add(str);
        }
        JsonArray jsList = jsb.build();

        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_TYPE, element.getType())
                .add(JSON_TEXT, element.getText())
                .add(JSON_LIST, jsList)
                .add(JSON_HEIGHT, element.getHeight())
                .add(JSON_WIDTH, element.getWidth())
                .add(JSON_SOURCE, element.getSource())
                .add(JSON_CAPTION, element.getCaption())
                .add(JSON_CAPTION, new SlideShowFileManager().makeSlideshowJson(element.getSlideShow()))
                .build();
        return jso;
    }
}
