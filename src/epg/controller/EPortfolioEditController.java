package epg.controller;



import static epg.StartupConstants.*;
import epg.model.EPortfolioModel;
import epg.view.EPortfolioGeneratorView;

/**
 * This controller provides responses for the eportfolio edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & _____________
 */
public class EPortfolioEditController {
    // APP UI
    private EPortfolioGeneratorView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public EPortfolioEditController(EPortfolioGeneratorView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddPageRequest() {
	EPortfolioModel ePortfolio = ui.getEPortfolio();
	//PropertiesManager props = PropertiesManager.getPropertiesManager();
	ePortfolio.addPage();
        
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wishes to remove it from the slide show.
     */
    public void processRemovePageRequest() {
	EPortfolioModel eportfolio = ui.getEPortfolio();
	eportfolio.removeSelectedPage();
	ui.reloadSiteToolbarPane();
        ui.reloadWorkspace();
    }


}
