/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author JIAQI ZHANG
 */
public class AddComponentDialog {

    Stage stage;
    Scene scene;
    VBox root;
    Button okButton;
    Button cancelButton;
    HBox buttonHbox;

    HBox listHbox;
    ObservableList<String> listItem;

    private EPortfolioGeneratorView ui;

    /**
     * This constructor keeps the UI for later.
     */
    public AddComponentDialog(EPortfolioGeneratorView initUI) {
        this.listItem = FXCollections.observableArrayList();
        ui = initUI;
        okButton = new Button("OK");
        cancelButton = new Button("Cancel");
        buttonHbox = new HBox(okButton, cancelButton);
        buttonHbox.setAlignment(Pos.CENTER);
        buttonHbox.setSpacing(10);
        buttonHbox.setPadding(new Insets(5, 0, 5, 0));
        stage = new Stage();
        root = new VBox();
        scene = new Scene(root);
    }

    void addHeadingDialog() {
        Label headingLabel = new Label("Heading: ");
        headingLabel.setFont(new Font("Arial", 18));
        TextField headingTextField = new TextField();
        headingTextField.setFont(new Font(16));
        headingTextField.setPrefSize(400, 30);
        headingTextField.setFont(new Font("Arial", 18));
        VBox vBox = new VBox();
        vBox.getChildren().addAll(headingLabel, headingTextField);
        root.getChildren().addAll(vBox, buttonHbox);

        okButton.setOnAction(e -> {
            Label lb = new Label("A heading was Added!");
            ui.pageView.getChildren().add(lb);
            stage.close();
        });
        cancelButton.setOnAction(e -> {
            stage.close();
        });

        stage.setTitle("Add Heading");
        stage.setScene(scene);
        stage.showAndWait();

    }

    void addParagraphDialog() {
        Label fontLabel = new Label("Font:");
        fontLabel.setFont(new Font("Arial", 18));
        ComboBox fontComboBox = new ComboBox();
        for (int i = 0; i < 5; i++) {
            fontComboBox.getItems().add(ui.fontComboBox.getItems().get(i));
        }
        fontComboBox.setValue(fontComboBox.getItems().get(0));
        Label paragraphLabel = new Label("Paragraph: ");
        paragraphLabel.setFont(new Font("Arial", 18));
        final TextArea paragraphTextArea = new TextArea();
        paragraphTextArea.setPrefSize(400, 250);
        paragraphTextArea.setWrapText(true);
        paragraphTextArea.setFont(new Font(16));
        VBox vBox = new VBox();
        vBox.getChildren().addAll(fontLabel, fontComboBox, paragraphLabel, paragraphTextArea);
        root.getChildren().addAll(vBox, buttonHbox);

        okButton.setOnAction(e -> {
            Label lb = new Label("A paragraph was Added!");
            ui.pageView.getChildren().add(lb);
            stage.close();
        });
        cancelButton.setOnAction(e -> {
            stage.hide();
        });

        stage.setTitle("Add Paragraph");
        stage.setScene(scene);
        stage.showAndWait();

    }

    void addListDialog() {
        stage.setWidth(300);
        stage.setHeight(500);
        loadListItem();
        stage.showAndWait();
    }

    void addImageDialog() {
        stage.setWidth(300);
        stage.setHeight(300);
        Label heightLabel = new Label("Height: ");
        heightLabel.setFont(new Font("Arial", 18));
        TextField heightTextField = new TextField();

        Label widthLabel = new Label("Width: ");
        widthLabel.setFont(new Font("Arial", 18));
        TextField widthTextField = new TextField();

        Label captionLabel = new Label("Caption: ");
        captionLabel.setFont(new Font("Arial", 18));
        TextField captionTextField = new TextField();
        
        Label srcLabel = new Label("Source: ");
        srcLabel.setFont(new Font("Arial", 18));
        Button chooseButton = new Button("Choose File");
        ;

        VBox vBox = new VBox();
        vBox.getChildren().addAll(heightLabel, heightTextField,
                widthLabel, widthTextField,
                captionLabel, captionTextField,
                srcLabel, chooseButton);
        root.getChildren().addAll(vBox, buttonHbox);

        okButton.setOnAction(e -> {
            Label lb = new Label("An Image was Added!");
            ui.pageView.getChildren().add(lb);
            stage.close();
        });
        cancelButton.setOnAction(e -> {
            stage.close();
        });

        stage.setTitle("Add Image");
        stage.setScene(scene);
        stage.showAndWait();
    }

    void addVideoDialog() {
        stage.setWidth(300);
        stage.setHeight(300);
        Label heightLabel = new Label("Height: ");
        heightLabel.setFont(new Font("Arial", 18));
        TextField heightTextField = new TextField();

        Label widthLabel = new Label("Width: ");
        widthLabel.setFont(new Font("Arial", 18));
        TextField widthTextField = new TextField();

        Label captionLabel = new Label("Caption: ");
        captionLabel.setFont(new Font("Arial", 18));
        TextField captionTextField = new TextField();
        
        Label srcLabel = new Label("Source: ");
        srcLabel.setFont(new Font("Arial", 18));
        Button chooseButton = new Button("Choose File");
        ;

        VBox vBox = new VBox();
        vBox.getChildren().addAll(heightLabel, heightTextField,
                widthLabel, widthTextField,
                captionLabel, captionTextField,
                srcLabel, chooseButton);
        root.getChildren().addAll(vBox, buttonHbox);

        okButton.setOnAction(e -> {
            Label lb = new Label("A video was Added!");
            ui.pageView.getChildren().add(lb);
            stage.close();
        });
        cancelButton.setOnAction(e -> {
            stage.close();
        });

        stage.setTitle("Add Video");
        stage.setScene(scene);
        stage.showAndWait();
    }

    void addSlideshowDialog() {
        
    }

    private void loadListItem() {
        root.getChildren().clear();
        Label listLabel = new Label("List:");
        listLabel.setFont(new Font("Arial", 16));
        listLabel.setPadding(new Insets(5, 0, 5, 0));
        root.getChildren().add(listLabel);
        Label itemLabel;
        TextField itemTextField;
        Button removeButton;
        Button addButton;
        for (String s : listItem) {
            itemLabel = new Label("Item: ");
            itemTextField = new TextField(s);
            removeButton = new Button("Remove");
            listHbox = new HBox(itemLabel, itemTextField, removeButton);
            root.getChildren().add(listHbox);

            removeButton.setOnAction(e -> {
                root.getChildren().remove(listHbox);
            });

        }

        addButton = new Button("Add Item");
        addButton.setOnAction(e -> {
            listItem.add("");
            loadListItem();
        });
        root.getChildren().add(addButton);
        root.getChildren().add(buttonHbox);

        okButton.setOnAction(e -> {
            Label lb = new Label("A list was Added!");
            ui.pageView.getChildren().add(lb);
            stage.close();
        });
        cancelButton.setOnAction(e -> {
            stage.hide();
        });

        stage.setTitle("Add List");
        stage.setScene(scene);

    }

}
